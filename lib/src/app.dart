import 'package:flutter/material.dart';
import 'package:flutter_p1/pages/contador_page.dart';
//import 'package:flutter_p1/pages/home_page.dart';

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Center(
        child: ContadorPage() 
        //child: HomePage() 
      ),
      theme: ThemeData(
        primarySwatch: Colors.lime,
      )
    );
  }
}
