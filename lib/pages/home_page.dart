import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  
  final _count = 0;
  final txtStyle = new TextStyle(fontSize: 25.0);

  @override
  Widget build(BuildContext context ) {
    
    return Scaffold( 
      appBar: AppBar(
        backgroundColor: Colors.amber,         
        title: Text('Contador'),
        centerTitle: false, 
        elevation: 10,
      ),
      body: Center( 
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Hola Mundo!', style: txtStyle ),
            Text('$_count', style: txtStyle)
          ],
        )
      ),
      floatingActionButton: FloatingActionButton(        
      onPressed: () => print('Hola Mundo!'),
      foregroundColor: Colors.greenAccent,
      tooltip: 'Increment Counter',
      child: const Icon(Icons.send),
    ),
    floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}