import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ContadorPageState();
  }
}

class _ContadorPageState extends State<ContadorPage> {
  int _contador = 3800;
  String _nombre = '®';
  final txtStyle = new TextStyle(fontSize: 25.0, color: Colors.pink, 
  fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text('ROXELY POL * Contador'),
        centerTitle: false,
        elevation: 10,
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
            Text('Hola $_nombre!', style: txtStyle),
            Text('$_contador', style: txtStyle)
          ])),
      floatingActionButton: _floatingActionButtons(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  void _sumarRoxely() {
    _contador = _contador + 2;
    _nombre = 'Roxely';
    print('$_contador Roxely');
    setState(() {});
  }

  void sumarPol() {
    _contador++;
    _nombre = 'Pol';
    print('$_contador Pol');
    setState(() {});
  }

  Widget _floatingActionButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(width: 10),
        FloatingActionButton(
          child: Icon(Icons.book),
          onPressed: _sumarRoxely,
          foregroundColor: Colors.pinkAccent,
          tooltip: 'Increment Counter Rox',
        ),
        Expanded(
            child: SizedBox(
          width: 1.0,
        )),
        FloatingActionButton(
          child: Icon(Icons.chrome_reader_mode),
          onPressed: sumarPol,
          foregroundColor: Colors.blueAccent,
          tooltip: 'Increment Counter',
        ),
        Expanded(
            child: SizedBox(
          width: 1.0,
        )),
        FloatingActionButton(
          child: Icon(Icons.brush),
          onPressed: () {
            _contador++;
            _nombre = 'VALENTINA';
            print('$_contador VALENTINA');
            setState(() {});
          },
          foregroundColor: Colors.purple,
          tooltip: 'Increment Counter',
        ),
        SizedBox(width: 10),
      ],
    );
  }
}
